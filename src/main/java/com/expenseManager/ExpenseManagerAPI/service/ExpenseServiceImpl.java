package com.expenseManager.ExpenseManagerAPI.service;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.expenseManager.ExpenseManagerAPI.domain.Expense;
import com.expenseManager.ExpenseManagerAPI.repositories.ExpenseRepository;

@Service
public class ExpenseServiceImpl implements Expenseservice {

	@Autowired
	ExpenseRepository expenseRepository;

	@Override
	public List<Expense> findAll() {
		return expenseRepository.findAll();
	}

	@Override
	public List<Expense> findByMonthAndYear(String month, int year) {
		return expenseRepository.findByMonthAndYear(month, year);
	}
	
	@Override
	public List<Expense> findByYear(int year) {
		return expenseRepository.findByYear(year);
	}
	
	@Override
	public Optional<Expense> findById(String id) {
		return expenseRepository.findById(id);
	}
	@Override
	public void saveOrUpdateExpense(Expense expense) {
		expenseRepository.save(expense);
	}

	@Override
	public void deleteExpense(String id) {
		expenseRepository.deleteById(id);		
	}

	

}