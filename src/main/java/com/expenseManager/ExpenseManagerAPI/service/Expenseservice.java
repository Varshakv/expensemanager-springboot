package com.expenseManager.ExpenseManagerAPI.service;

import java.util.List;
import java.util.Optional;

import com.expenseManager.ExpenseManagerAPI.domain.Expense;

public interface Expenseservice {
	
	List<Expense> findAll();
	
	List<Expense> findByMonthAndYear(String month, int year);
	
	List<Expense> findByYear(int year);
	Optional<Expense> findById(String id);
	
	void saveOrUpdateExpense(Expense expense);
	
	void deleteExpense(String id);

}